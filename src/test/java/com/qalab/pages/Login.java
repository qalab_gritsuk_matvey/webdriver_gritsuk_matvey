package com.qalab.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.*;


public class Login extends Page {

    @FindBy(xpath = "//*[@id='gh-ug']/a")
    WebElement buttonStartLogIn;

    @FindBy(xpath = "//*[@type='text']")
    WebElement inputEmail;

    @FindBy(xpath = "//*[@type='password']")
    WebElement inputPassword;

    @FindBy(xpath = "//*[@id='sgnBt']")
    WebElement buttonEndLogIn;

    public Login(WebDriver driver) {
        super(driver);
    }

    /**
     * Searches the button "Sign in" and clicks it.
     */
    @Step("Clicks the button 'Sign in'")
    public void startLogIn() {
        buttonStartLogIn.click();
    }

    /**
     * Searches the string for email and inputs the email from parameter of this method.
     * @param Email
     */
    @Step("Inputs email of user")
    public void inputEmail(String Email) {
        inputEmail.sendKeys(Email);
    }

    /**
     * Searches the string for password and inputs the password.
     * @throws IOException
     */
    @Step("Input password of user")
    public void inputPassword() throws IOException {

        inputPassword.sendKeys("kanatohodets5436");
    }

    /**
     * Searches the button "Sign in" and clicks it.
     */
    @Step("Clicks the button 'Sign in' and completes the login process ")
    public void endLogIn(){
        buttonEndLogIn.click();
    }

}


package com.qalab.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

public class SearchFormPage extends Page {

    @FindBy(xpath = "//*[@id='gh-btn']")
    WebElement buttonSearch;

    @FindBy(xpath = "//*[@id='gh-ac']")
    WebElement inputSearch;

    public SearchFormPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Searches a search line and inputs the query from parameter of the method.
     * @param Query
     */
    @Step("Inputs query in the search line ")
    public void inputQueryInTheSearchBoxAndClickTheSearchButton(String Query){
        inputSearch.sendKeys(Query);
    }

    /**
     * Searches the button "Search" and clicks it.
     */
    @Step("Clicks the button 'Search'")
    public void clickButtonSearch() {
        buttonSearch.click();
    }

}
package com.qalab;


import com.qalab.pages.*;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

import java.io.*;

public class SampleTestNgTest extends TestNgTestBase {

  private HomePage homepage;
  private SearchFormPage searchformpage;
  private ItemsInCart itemsincart;
  private Login login;
  private SerpPage serppage;

  @BeforeTest
  public void initPageObjects() {
    homepage = PageFactory.initElements(driver, HomePage.class);
    searchformpage = PageFactory.initElements(driver, SearchFormPage.class);
    login = PageFactory.initElements(driver, Login.class);
    itemsincart = PageFactory.initElements(driver, ItemsInCart.class);
    serppage = PageFactory.initElements(driver, SerpPage.class);
  }

  @BeforeClass
  public void logInAndSearchItemsAndGetLinksOfItemsAndCleanCart() throws IOException {
    homepage.open();
    login.startLogIn();
    login.inputEmail("oneloveonemotiv@gmail.com");
    login.inputPassword();
    login.endLogIn();
    searchformpage.inputQueryInTheSearchBoxAndClickTheSearchButton("globe");
    searchformpage.clickButtonSearch();
    serppage.clickButtonBuyNow();
    serppage.getListOfLinks();
    serppage.openCart();
    itemsincart.deleteAllItemsFromCart();
  }

  @Stories("Check price of cart after adding one item")
  @Severity(SeverityLevel.BLOCKER)
  @Test(description = "Check that price of one item coincides with intermediate price in cart")
  public void checkPriceOfOneItemCoincidesWithIntermediatePriceInTheCart(){
    serppage.addToCartOneItem();
    itemsincart.checkThatPriceOfOneItemCoincidesWithIntermediatePrice();
  }

  @Stories("Check price of cart after adding a few items")
  @Severity(SeverityLevel.BLOCKER)
  @Test(description = "Check that total price of items coincides with intermediate price in cart")
  public void checkTotalPriceOfItemsCoincidesWithIntermediatePriceInCart(){
    serppage.addToCartAFewItems();
    itemsincart.checkThatTotalPriceOfItemsCoincidesWithIntermediatePrice();
  }

  @Stories("Check price of empty cart")
  @Severity(SeverityLevel.BLOCKER)
  @Test(description = "Check that price of empty cart is zero")
  public void checkThatPriceOfEmptyCartIsZero(){
    serppage.openCart();
    itemsincart.checkThatPriceOfEmptyCartIsZero();
  }

  @Stories("Check price of cart after removing one item")
  @Severity(SeverityLevel.CRITICAL)
  @Test(description = "Check that total price of items coincides with intermediate price in cart after deleting one item")
  public void checkThatTotalPriceOfItemsCoincidesWithIntermediatePriceInCartAfterDeletingOneItem(){
    serppage.addToCartAFewItems();
    itemsincart.deleteOneItem();
    itemsincart.checkThatTotalPriceOfItemsCoincidesWithIntermediatePrice();
  }

  @Stories("Check price of cart after increasing the amount of instance")
  @Severity(SeverityLevel.CRITICAL)
  @Test(description = "Check that total price of items coincides with intermediate price in cart after increasing the amount of instance")
  public void checkPriceOfTwoSimilarItemsPlusPriceOfAnotherThingCoincideWithIntermediatePriceInCart(){
    serppage.addToCartAFewItems();
    itemsincart.changeTheAmountOfInstance("2");
    itemsincart.checkThatTotalPriceOfItemsCoincidesWithIntermediatePrice();
  }

  @Stories("Check price of cart after reducing the amount of instance")
  @Severity(SeverityLevel.NORMAL)
  @Test(description = "Check that total price of items coincides with intermediate price in cart after reducing the amount of instance")
  public void checkThatIntermediatePriceUpdateAfterRemovingOneInstance(){
    serppage.addToCartAFewItems();
    itemsincart.changeTheAmountOfInstance("2");
    itemsincart.changeTheAmountOfInstance("1");
    itemsincart.checkThatTotalPriceOfItemsCoincidesWithIntermediatePrice();
  }

  @Stories("Check price of cart after saving one item for the later")
  @Severity(SeverityLevel.NORMAL)
  @Test(description = "Check that total price of items coincides with intermediate price in cart after saving one item for the later")
  public void checkThatTotalPriceOfItemsCoincidesWithIntermediatePriceAfterSavingOneItemForTheLater(){
    serppage.addToCartAFewItems();
    itemsincart.saveOneItemForLater();
    itemsincart.checkThatPriceOfOneItemCoincidesWithIntermediatePrice();
  }

  @Stories("Check price of cart after returning one item in the cart")
  @Severity(SeverityLevel.NORMAL)
  @Test(description = "Check that total price of items coincides with intermediate price in cart after returning one item in the cart")
  public void CheckThatTotalPriceOfItemsCoincidesWithIntermediatePriceAfterReturningOneItemInTheCart(){
    serppage.addToCartAFewItems();
    itemsincart.saveOneItemForLater();
    itemsincart.returnSavedItemInCart();
    itemsincart.checkThatTotalPriceOfItemsCoincidesWithIntermediatePrice();
  }

  @Stories("Check price of cart after equating the amount of one item to zero")
  @Severity(SeverityLevel.NORMAL)
  @Test(description = "Check that total price of items coincides with intermediate price in cart after equating the amount of one item to zero")
  public void checkThatTotalPriceOfItemsCoincidesWithIntermediatePriceAfterEquatingTheAmountOfOneItemToZero(){
    serppage.addToCartAFewItems();
    itemsincart.changeTheAmountOfInstance("0");
    itemsincart.checkThatTotalPriceOfItemsCoincidesWithIntermediatePrice();
  }

  @Stories("Check price of cart is zero after deleting all items")
  @Severity(SeverityLevel.NORMAL)
  @Test(description = "Check that price of cart is zero after deleting all items")
  public void checkThatPriceOfCartIsZeroAfterDeletingAllItems(){
    serppage.addToCartAFewItems();
    itemsincart.deleteAllItemsFromCart();
    itemsincart.checkThatPriceOfEmptyCartIsZero();
  }

  @AfterMethod
  public void cleanCart(){
    itemsincart.deleteAllItemsFromCart();
  }
}

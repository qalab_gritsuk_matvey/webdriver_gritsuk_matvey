package com.qalab.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

public class ItemsInCart extends Page {

    @FindBy(xpath = "//*[@class='fw-b']")
    List<WebElement> totalPriceOfItemsInCart;

    @FindBy(xpath = "//*[@id='asynccartsummary']/div[1]/table/tbody/tr[1]/td[2]/div")
    WebElement intermediatePrice;

    @FindBy(xpath = "//*[@class='clr333  normal ']")
    WebElement priceOfEmptyCart;

    @FindBy(xpath = "//*[@aria-label='Save for later']")
    WebElement saveForLater;

    @FindBy(xpath = "//*[@aria-label='Add back to cart']")
    WebElement returnSavedItemInTheCart;

    @FindBy(xpath = "//*[@class='ff-ds3 fs12 qtyTextBox']")
    List<WebElement> inputValues;

    @FindBy(xpath = "//*[@class='uqtyLink']")
    WebElement buttonUpdateAmountOfInstance;

    @FindBy(xpath = "//*[@aria-label='Remove']")
    WebElement deleteItem;

    public ItemsInCart(WebDriver driver) {
        super(driver);
    }

    /**
     * This method searches the list of the values of the amount of each item.
     * Gets the value of the first element of the list. Clears the value of the first element.
     * Adds here the value of parameter of the method.
     * Searches the button "update" in the cart and clicks it.
     * @param number
     */
    @Step("Changes the amount of instance")
    public void changeTheAmountOfInstance(String number){
        inputValues.get(0).clear();
        inputValues.get(0).sendKeys(number);
        buttonUpdateAmountOfInstance.click();
    }

    /**
     * The method searches the button "Save for later" in the cart of "ebay" and clicks.
     */
    @Step("Saves the item for later")
    public void saveOneItemForLater(){
        saveForLater.click();

    }

    /**
     * The method searches the button "Add back to cart" in the cart of "ebay" and clicks.
     */
    @Step("Returns saved item in cart")
    public void returnSavedItemInCart(){
        returnSavedItemInTheCart.click();
    }

    /**
     * This method searches the button "delete" in the cart of "ebay".
     * If such button exists, the method clicks it. Then checks for the same button. And again clicks, if button exists.
     * If the method doesn't find the button, it stops executing.
     */
    @Step("Removes all items from cart")
    public void deleteAllItemsFromCart() {
        while(true){
            try {
                deleteItem.click();
            } catch (Exception ex) {
                break;
            }
        }
    }

    /**
     * This method delete the first item in the cart of "ebay".
     */
    @Step("Removes one item")
    public void deleteOneItem(){
        deleteItem.click();
    }

    /**
     * This method gets the list with the values of the price of each item in the cart of "ebay".
     * Then the method adds these values and gets the value of intermediate price of the cart.
     * Then the method compares the intermediate price with the total price of items.
     * If intermediate price equal the total price of items - true.
     * If intermediate price doesn't equal the price of items - false.
     */
    @Step("Checks that total price of items coincides with intermediate price")
    public void checkThatTotalPriceOfItemsCoincidesWithIntermediatePrice(){
        double n = 0;
        for (WebElement totalPrice : totalPriceOfItemsInCart) {
            n = n + new Double(totalPrice.getText().replace(",", ".").replaceAll("^[\\D]+", ""));
        }
        Assert.assertEquals(Double.parseDouble(intermediatePrice.getText().replace(",", ".").replaceAll("^[\\D]+", "")), n);
    }

    /**
     * This method gets the value of intermediate price and the value of the price of one item in the cart of "ebay" and compares these values.
     * If intermediate price equal the price of one item - true.
     * If intermediate price doesn't equal the price of one item - false.
     */
    @Step("Checks that total price of ONE item coincides with intermediate price")
    public void checkThatPriceOfOneItemCoincidesWithIntermediatePrice(){
        Assert.assertEquals(intermediatePrice.getText().toLowerCase(), totalPriceOfItemsInCart.get(0).getText().toLowerCase());
    }


    /**
     * This method gets the value of the price in the cart of "ebay" when the cart is empty and compares this value with 0.
     * If the value is 0 - true.
     * If the value isn't 0 - false.
     */
    @Step("Checks that price of empty cart is zero")
    public void checkThatPriceOfEmptyCartIsZero(){
        Assert.assertEquals(Integer.parseInt(priceOfEmptyCart.getText().replaceAll("[\\D]+", "")), 0);
    }

}

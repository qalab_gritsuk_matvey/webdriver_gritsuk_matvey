package com.qalab.pages;

import com.qalab.util.PropertyLoader;
import org.openqa.selenium.WebDriver;
import java.lang.String;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import java.io.IOException;

public class HomePage extends Page {

  @FindBy(how = How.TAG_NAME, using = "h1")
  protected static String baseURL;

  public HomePage(WebDriver webDriver) {
    super(webDriver);
  }

  /**
   * Gets the url from pom.xml.
   * Opens webpage with this url.
   * @throws IOException
   */
  public void open() throws IOException{
    baseURL = PropertyLoader.loadProperty("site.url");
    driver.get(baseURL);
  }
}

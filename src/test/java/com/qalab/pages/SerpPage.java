package com.qalab.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

public class SerpPage extends Page {

    @FindBy(xpath = "//*[@id='cbelm']/div[1]/div[2]/a[2]")
    WebElement buttonBuyNow;

    @FindBy(xpath = "//*[@id='ListViewInner']//h3//a")
    List<WebElement> headers;

    @FindBy(xpath = "//*[@id='isCartBtn_btn']")
    WebElement buttonAddToCart;

    @FindBy(xpath = "//*[@id='gh-cart-i']")
    WebElement buttonEnterToCart;

    List<String> resultList = new ArrayList();

    public SerpPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Searches the button "Buy now" and clicks it.
     */
    @Step("Clicks the button 'Buy now'")
    public void clickButtonBuyNow() {
        buttonBuyNow.click();
    }

    /**
     * Searches the list of snippets.
     * Gets attribute "href" of each snippet.
     * Adds all the attributes "href" to other list "resultList".
     */
    @Step("Gets a list of links of snippets")
    public void getListOfLinks() {

        for (WebElement header : headers) {
            if (header.getAttribute("href") != null)
                resultList.add(header.getAttribute("href"));
        }
    }

    /**
     * Gets element "href" of the list "resultList". Opens this url.
     * Searches the button "Add to cart".
     * If the button exists, the method clicks it.
     * If the button doesn't exist, the method gets the next element of the list.
     * The method is completed when the button "Add to cart" will be clicked once.
     */
    @Step("Adds to cart one item")
    public void addToCartOneItem() {

        for(String href:resultList){
            driver.get(href);
            try {
                buttonAddToCart.click();
                break;
            } catch (Exception ex){ex.printStackTrace();}
        }
    }

    /**
     * Gets element "href" of the list "resultList". Opens this url.
     * Searches the button "Add to cart".
     * If the button exists, the method clicks it.
     * If the button doesn't exist, the method gets the next element of the list.
     * The method is completes when the button "Add to cart" will be clicked twice.
     */
    @Step("Adds to cart a few items")
    public void addToCartAFewItems(){
        for(int i = 0, k = 0; i<resultList.size() & k<2; i++){
            driver.get(resultList.get(i));
            try{
                buttonAddToCart.click();
                k++;
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    /**
     * Searches the button "Cart" and clicks it.
     */
    @Step("Clicks the button 'cart' and opens cart'")
    public void openCart(){
        buttonEnterToCart.click();
    }

}

